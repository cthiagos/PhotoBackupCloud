package br.edu.unyleya.photobackupcloud.service;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by cthiagos on 22/11/2017.
 */
public class PhotoService {

    private String photoAbsolutePath;
    private String photoName;

    public PhotoService(String photoAbsolutePath) {

        this.photoAbsolutePath = photoAbsolutePath;
        this.photoName = getPhotoName();
    }

    public void save() throws IOException {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        StorageReference photoCloudRef = storageRef.child("photos/" + photoName);
        InputStream stream = new FileInputStream(new File(photoAbsolutePath));
        UploadTask uploadTask = photoCloudRef.putStream(stream);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Manuseie os uploads mal sucedidos
                exception.printStackTrace();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contém metadados de arquivos, como tamanho,
                // tipo de conteúdo e URL de download.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
            }
        });
    }

    public String getPhotoName() {
        if (photoName == null && photoAbsolutePath != null) {
            photoName = photoAbsolutePath.substring(photoAbsolutePath.lastIndexOf("/") + 1, photoAbsolutePath.length());
        }
        return photoName;
    }
}
package br.edu.unyleya.photobackupcloud;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import br.edu.unyleya.photobackupcloud.service.PhotoService;


public class MainActivity extends Activity {

    PhotoService photoService;
    private ArrayList<String> photos;
    private int position = Integer.MIN_VALUE;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GridView gallery = findViewById(R.id.galleryGridView);
        gallery.setAdapter(new ImageAdapter(this));
        gallery.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                if (null != photos && !photos.isEmpty()) {
                    setPosition(position);
                    System.out.println("position " + position + " " + photos.get(position));
                }
            }
        });
    }

    public void save(View v) {
        try {
            photoService = new PhotoService(photos.get(getPosition()));
            photoService.save();

            Toast.makeText(MainActivity.this,
                    "Foto " + photoService.getPhotoName() + " gravada com sucesso!",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this,
                    "Erro ao gravar: " + ex.getMessage(),
                    Toast.LENGTH_LONG).show();
        }
    }

    private class ImageAdapter extends BaseAdapter {

        private Activity context;

        public ImageAdapter(Activity localContext) {
            context = localContext;
            loadArrayImages(context);
        }

        public int getCount() {
            return photos.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ImageView picturesView;
            if (convertView == null) {
                picturesView = new ImageView(context);
                picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                picturesView.setLayoutParams(new GridView.LayoutParams(270, 270));
            } else {
                picturesView = (ImageView) convertView;
            }
            Glide.with(context).load(photos.get(position))
                    .placeholder(R.mipmap.ic_launcher).centerCrop()
                    .into(picturesView);

            return picturesView;
        }

        private void loadArrayImages(Activity activity) {
            photos = new ArrayList<String>();
            Uri uri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

            String[] projection = {MediaColumns.DATA,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

            Cursor cursor = activity.getContentResolver().query(uri, projection, null,
                    null, null);

            int column_index_data = cursor.getColumnIndexOrThrow(MediaColumns.DATA);

            while (cursor.moveToNext()) {
                photos.add(cursor.getString(column_index_data));
            }
            cursor.close();
        }
    }
}